import os
 
import ament_index_python.packages
import launch
from launch_ros.actions import Node
 
 
def generate_launch_description():
    config_directory = os.path.join(
        ament_index_python.packages.get_package_share_directory('ublox_gps'),
        'config')
    params = os.path.join(config_directory, 'zed_f9p.yaml')
    ublox_gps_node = Node(package='ublox_gps',
                            executable='ublox_gps_node',
                            output='screen',
                            parameters=[params],
                            remappings=[('/ublox_gps_node/fix', '/fix'),
                                        ('/ublox_gps_node/fix_velocity', '/fix_velocity'),]
                            )
 
    return launch.LaunchDescription(
        [ublox_gps_node,
         launch.actions.RegisterEventHandler(
            event_handler=launch.event_handlers.OnProcessExit(
                target_action=ublox_gps_node,
                on_exit=[launch.actions.EmitEvent(event=launch.events.Shutdown())],
                )
            ),
        ])