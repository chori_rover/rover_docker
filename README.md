# Dockerfiles para la Plataforma Terrestre de usos generales ROVER

En este repo tenemos los Dockerfiles de los diferentes packetes/drivers del sistema.
Para el armado de las imágenes se utilizó una técnica que permite reducir el tamaño de la imagen final. Para ello se comienza con una imagen de ROS con packetes preinstalados como es FROM ros:iron. Aquí se clonan los repositorios necesarios, se instalan las dependencias necesarias y se compila el paquete. Luego se pasa a una nueva imagen, más pequeña, como ser FROM ros:iron-ros-core, aquí se copia el ws creado anteriormente (sólo el install), se instala, de ser necesaria, alguna dependencia necesaria para correr el driver y se finaliza la configuración de la imagen. De este modo, la imagen final es lo más pequeña posible.

## Rover

En rover tenemos el Dockerfile correspondiente a los nodo que corresponden al rover propiamente dicho. Se utilizan los repos desarrollados [rover](https://gitlab.com/elgarbe/rover.git) y [rover description]( https://gitlab.com/elgarbe/rover_description.git)

## GPS ublox f9p

En ublox_gps tenemos el Dockerfile con los drivers del GPS RTK, en este caso los provistos por [KumarRobotics](https://github.com/KumarRobotics/ublox/tree/ros2)

## LIDAR RPLidar S1

En rplidar tenemos el Dockerfile con los drivers del LIDAR, en este caso los provistos por [SLAMTEC](https://github.com/Slamtec/sllidar_ros2)

## Micro-ROS

Para ejecutar el agente de micro-ros usar la [imagen de docker](https://hub.docker.com/r/microros/micro-ros-agent) que ya viene pre-compilada.

## Compilar y ejecutar

Se provee un docker-compose que permite generar todas las imágenes (excepto la de micro-ros, la cual usamos la de dockerhub) y correrlas.
Simplemente en el raíz del repo ejecutar

```bash
docker compose up -d 
```
## Restart policy

Para conseguir que los nodos (drivers) levanten cuando se bootea la Jetson Nano, se configura la [restart policy](https://docs.docker.com/config/containers/start-containers-automatically/#use-a-restart-policy).

